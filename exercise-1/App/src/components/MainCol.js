import React from "react";
import { UserInfo } from "./user/UserInfo";
import { UserName } from "./user/UserName";

export const MainCol = ({
  name,
  city,
  street,
  suite,
  email,
  phone,
  company,
}) => {
  return (
    // In this Component, it will received the infos and name from the user in props and then pass it to different components to display them and reuse if necessary with other infos
    // The address will display, only if all the props are available

    <div className="w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0">
      <div className="p-4 md:p-12 text-center lg:text-left">
        <UserName name={name} />
        <UserInfo
          infoType={"Address"}
          infoData={city && street && suite && `${city}, ${street}, ${suite}`}
        />
        <UserInfo infoType={"Email"} infoData={email} />
        <UserInfo infoType={"Phone"} infoData={phone} />
        <UserInfo infoType={"Company"} infoData={company} />
      </div>
    </div>
  );
};
