import React from "react";
import { useQuery, gql } from "@apollo/client";
import Loading from "./Loading";
import ErrorComponent from "./ErrorComponent";
import { MainCol } from "./MainCol";
import { PostsCol } from "./PostsCol";

//With the above query I get the infos from the user with Id 1, as well as the last 10 posts from him
const GET_USER = gql`
  {
    user(id: 1) {
      id
      name
      address {
        city
        suite
        street
      }
      email
      phone
      company {
        name
      }
      posts(options: { paginate: { page: 1, limit: 10 } }) {
        data {
          id
          title
          body
        }
      }
    }
  }
`;

const LandingPage = () => {
  const { loading, error, data } = useQuery(GET_USER);
  //While get the info from the graphQL API, it will render the Loading Component
  if (loading) return <Loading />;

  // If some error get from the graphQL API, it will render the ErrorComponent Component
  if (error) return <ErrorComponent error={error} />;

  // When received the data from the graphQL API, and no error encontered, it will render the above components and pass the necessary props to each component
  // MainCol is the collumn on the left (in HD) with the user infos and the PostsCol is the collumn on the right (in HD) with the user's posts
  return (
    <div className="max-w-4xl flex items-center h-auto lg:h-screen flex-wrap mx-auto my-32 lg:my-0">
      <MainCol
        name={data.user.name}
        city={data.user.address.city}
        street={data.user.address.street}
        suite={data.user.address.suite}
        email={data.user.email}
        phone={data.user.phone}
        company={data.user.company.name}
      />
      <PostsCol name={data.user.name} posts={data.user.posts.data} />
    </div>
  );
};

export default LandingPage;
