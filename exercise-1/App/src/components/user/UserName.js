import React from "react";

export const UserName = ({ name }) => {
  return (
    //This component will display the user's name
    <>
      <h1 className="text-3xl font-bold pt-8 lg:pt-0">
        {name ? name : "Name unavailable"}
      </h1>
      <div className="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-teal-500 opacity-25"></div>
    </>
  );
};
