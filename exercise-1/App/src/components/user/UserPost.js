import React from "react";

export const UserPost = ({ title }) => {
  return (
    <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start" >
      Title : {title}
    </p>
  );
};
