import React from "react";

export const UserInfo = ({ infoType, infoData }) => {
  //In this component it will received the infos from the props and display it.
  // I assumed that the Address would always be the first and then, the first info will not have the margin-top property, unless it has other name than Address
  return (
    <>
      <div
        className={`pt-4 text-base font-bold flex items-center justify-center lg:justify-start ${
          infoType !== "Address" && "mt-4"
        }`}
      >
        {infoType}:
      </div>
      <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
        {infoData ? infoData : "Info Unavailable"}
      </p>
    </>
  );
};
