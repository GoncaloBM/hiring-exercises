const fs = require("fs");
const Stripe = require("stripe");
// The stripe key must be hidden in gitignore, so that it won't be catch by other users when sent to bitbucket, but left it here so that you can test the exercise
// const STRIPE_TEST_SECRET_KEY = require("./key");
const STRIPE_TEST_SECRET_KEY =
  "rk_test_51EDa6GIrkMUcWfFwXon9vsJYpTqX2eTqbINAUf4fZC7ivToWv59cAPoHdYhmszwL9ZKWJtUUbaCcHtpjl6rWlXLP00C1dcAoUR";
const stripe = Stripe(STRIPE_TEST_SECRET_KEY);

const filterCustomersCountry = (json, country) => {
  //This function will receive the customer list from the Json and the country to create user
  let customersByCountry = [];

  // This for loop will filter the list by the country entered previously
  for (let i = 0; i < json.length; i++) {
    if (country === json[i].country) {
      customersByCountry.push(json[i]);
    }
  }
  return customersByCountry;
};

const getCountryCode = (country) => {
  // This function will replace the name of the country with the respective code
  let rawCountryCodes = fs.readFileSync("countries-ISO3166.json");
  let codesJson = JSON.parse(rawCountryCodes);

  // As the json is in object, it needs to be transformed to an array, so we can find the country with a for loop. That what the next line does
  const jsonArray = Object.entries(codesJson);

  let code;

  // This for loop will get the code for the country
  for (let i = 0; i < jsonArray.length; i++) {
    if (jsonArray[i][1] === country) code = jsonArray[i][0];
  }

  return code;
};

const transformCustomers = async (customers, code) => {
  // This function will replace the country from the json with the respective country code
  try {
    let customersWithCode = await customers;
    for (let i = 0; i < customersWithCode.length; i++) {
      customersWithCode[i].country = code;
    }
    return customersWithCode;
  } catch (error) {
    throw error;
  }
};

const createCustomer = async (customerFromList) => {
  // this will create customers in the Stripe API with the received customer from the filtered list
  try {
    let customerData = await customerFromList;
    const customer = await stripe.customers.create({
      email: customerData.email,
      name: `${customerData.last_name} ${customerData.last_name}`,
      address: {
        line1: customerData.address_line_1,
        country: customerData.country,
      },
    });
  } catch (error) {
    throw error;
  }
};

const createCustomersList = async (listOfCustomers, number) => {
  // This will create all the customers from the filtered list and return only the created ones ( with limit)
  try {
    for (let i = 0; i < listOfCustomers.length; i++) {
      await createCustomer(listOfCustomers[i]);
    }
    const customers = await stripe.customers.list({ limit: number });
    return customers;
  } catch (error) {
    throw error;
  }
};

const transformCustomerList = async (list, countryCode) => {
  // This function will create the finalCustomer array with only the properties selected (email, id and country)
  let listTransformed = [];
  let customerList = await list.data;

  for (let i = 0; i < list.data.length; i++) {
    if (customerList[i].address.country === countryCode) {
      listTransformed.push({
        email: customerList[i].email,
        customerId: customerList[i].id,
        country: customerList[i].address.country,
      });
    }
  }
  return listTransformed;
};

const saveFinalCustomersToJson = (customers) => {
  // This will overwrite all the data in the existing json
  // If need to add to existing json, must do the readFileSync, push the finalCustomers and then writeFileSync
  // If the customers weren's saved in the API, a warning will display. If saved, all customers will be displayed in console
  if (customers) {
    let customersToJson = JSON.stringify(customers);
    fs.writeFileSync("final-customers.json", customersToJson);
    console.log(
      `These customers were saved in final-customers.json :`
    );
    console.log(
 customers
    );
  } else {
    console.log(`Error! No customers were saved in final-customers.json`);
  }
};

const handler = async (country) => {
  try {
    let finalCustomers = [];

    /* add code below this line */
    let rawCustomers = fs.readFileSync("customers.json");
    let customersJson = JSON.parse(rawCustomers);
    // The last 2 lines was to get the customers from the customers.json

    let filteredCustomers = filterCustomersCountry(customersJson, country); //this is customers from the json filtered by the country selected

    // If there are customers in the country, it will create in the stripe API, if not it will display a message
    if (filteredCustomers.length > 0) {
      let countryCode = getCountryCode(country); // This is the code from the country

      let customersWithCode = await transformCustomers(
        filteredCustomers,
        countryCode
      ); // This is customers filtered with the code, instead of the country name

      let customerList = await createCustomersList(
        customersWithCode,
        customersWithCode.length
      ); // This is the list with the customers from the Stripe Api

      finalCustomers = await transformCustomerList(customerList, countryCode); // Finally this is the final list of customers with the respective id, email and country code
      saveFinalCustomersToJson(finalCustomers); // This will save the final customers into the Json
    } else {
      console.log(`No customer in ${country} to create`);
    }

    /* add code above this line */
  } catch (e) {
    throw e;
  }
};
handler("Spain");
